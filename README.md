# README #

### This repo contains configuration files for theme building with Git and ThemeKit ###

* Pull this repository locally, and have it handy when starting work on any themes not already in a git repo
* V. 0.0.1

### How do I get set up? ###

* git clone git@bitbucket.org:swankyapple/theme-init.git
* copy directory contents (not including `.git` file) to new directory for use with git i.e. "my-theme-1/"
* copy config-example.yml to config.yml and enter the details for your theme. If duplicating from a live theme, ensure production variables are NEVER equal to live.

### Contribution guidelines ###

* If you want to make changes to this repo, make a fork, do the work, and submit a pull request.

### Who do I talk to? ###

* Ross Eastman
* other memebers of the SA dev team
